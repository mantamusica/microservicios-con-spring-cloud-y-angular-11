/**
 * 
 */
package com.formacionbdi.microservicios.api.examenes.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.microservicios.api.examenes.models.repository.AsignaturaRepository;
import com.formacionbdi.microservicios.api.examenes.models.repository.ExamenRepository;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Examen;
import com.formacionbdi.microservicios.commons.services.CommonServiceImpl;

/**
 * @author jcagigas
 *
 */
@Service
public class ExamenServiceImpl extends CommonServiceImpl<Examen, ExamenRepository>implements ExamenServiceI {

	@Autowired
	private AsignaturaRepository repositoryAsignatura;
	
	@Override
	@Transactional(readOnly = true)
	public List<Examen> findByNombre(String value) {
		return repository.findByNombre(value);
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Asignatura> findAllAsignaturas() {

		return repositoryAsignatura.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Iterable<Long> findExamenesIdsConRespuestasByPreguntaIds(Iterable<Long> preguntaIds) {
		// TODO Auto-generated method stub
		return repository.findExamenesIdsConRespuestasByPreguntaIds(preguntaIds);
	}

}
