-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.5.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para db_microservicios_examenes
CREATE DATABASE IF NOT EXISTS `db_microservicios_examenes` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `db_microservicios_examenes`;

-- Volcando estructura para tabla db_microservicios_examenes.asignaturas
CREATE TABLE IF NOT EXISTS `asignaturas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `padre_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK1a657vrlox5uthk8wbwrxh6e8` (`padre_id`),
  CONSTRAINT `FK1a657vrlox5uthk8wbwrxh6e8` FOREIGN KEY (`padre_id`) REFERENCES `asignaturas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.asignaturas: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `asignaturas` DISABLE KEYS */;
INSERT IGNORE INTO `asignaturas` (`id`, `nombre`, `padre_id`) VALUES
	(1, 'Matemáticas', NULL),
	(2, 'Lenguaje', NULL),
	(3, 'Inglés', NULL),
	(4, 'Ciencias Naturales', NULL),
	(5, 'Ciencias Sociales y Historia', NULL),
	(6, 'Música', NULL),
	(7, 'Artes', NULL),
	(8, 'Algebra', 1),
	(9, 'Aritmética', 1),
	(10, 'Trigonometría', 1),
	(11, 'Lectura y comprensión', 2),
	(12, 'Verbos', 2),
	(13, 'Gramática', 2),
	(14, 'Inglés', 3),
	(15, 'Gramática', 3),
	(16, 'Verbos', 3),
	(17, 'Ciencias Naturales', 4),
	(18, 'Biología', 4),
	(19, 'Física', 4),
	(20, 'Quimica', 4),
	(21, 'Historia', 5),
	(22, 'Ciencias Sociales', 5),
	(23, 'Filosofía', 5),
	(24, 'Música', 6),
	(25, 'Artes', 7);
/*!40000 ALTER TABLE `asignaturas` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.cursos
CREATE TABLE IF NOT EXISTS `cursos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` datetime(6) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.cursos: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT IGNORE INTO `cursos` (`id`, `create_at`, `nombre`) VALUES
	(1, '2020-12-23 09:42:49.000000', '1ª Primaria'),
	(2, '2020-12-23 09:42:55.000000', '2ª Primaria'),
	(3, '2020-12-23 09:43:01.000000', '3ª Primaria'),
	(4, '2020-12-23 09:43:10.000000', '1ª Secundaria'),
	(5, '2020-12-23 09:43:13.000000', '2ª Secundaria'),
	(6, '2020-12-23 09:43:16.000000', '3ª Secundaria'),
	(7, '2020-12-23 09:43:19.000000', '4ª Secundaria');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.cursos_alumnos
CREATE TABLE IF NOT EXISTS `cursos_alumnos` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alumno_id` bigint(20) DEFAULT NULL,
  `curso_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pb88irw9u3c0h2lsf3q1kluv9` (`alumno_id`),
  KEY `FKb90xg2w8jai6w555c3erim0cv` (`curso_id`),
  CONSTRAINT `FKb90xg2w8jai6w555c3erim0cv` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.cursos_alumnos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `cursos_alumnos` DISABLE KEYS */;
INSERT IGNORE INTO `cursos_alumnos` (`id`, `alumno_id`, `curso_id`) VALUES
	(1, 1, 7),
	(5, 7, 7);
/*!40000 ALTER TABLE `cursos_alumnos` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.cursos_examenes
CREATE TABLE IF NOT EXISTS `cursos_examenes` (
  `curso_id` bigint(20) NOT NULL,
  `examenes_id` bigint(20) NOT NULL,
  KEY `FK6ags9h8g0q074pch8ckfy8nw5` (`examenes_id`),
  KEY `FKbj3nwplxm8hswqcbt0tmrqagj` (`curso_id`),
  CONSTRAINT `FK6ags9h8g0q074pch8ckfy8nw5` FOREIGN KEY (`examenes_id`) REFERENCES `examenes` (`id`),
  CONSTRAINT `FKbj3nwplxm8hswqcbt0tmrqagj` FOREIGN KEY (`curso_id`) REFERENCES `cursos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.cursos_examenes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cursos_examenes` DISABLE KEYS */;
INSERT IGNORE INTO `cursos_examenes` (`curso_id`, `examenes_id`) VALUES
	(7, 1);
/*!40000 ALTER TABLE `cursos_examenes` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.examenes
CREATE TABLE IF NOT EXISTS `examenes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_at` datetime(6) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `asignatura_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6ti4mhut3mays6044rt8syqd8` (`asignatura_id`),
  CONSTRAINT `FK6ti4mhut3mays6044rt8syqd8` FOREIGN KEY (`asignatura_id`) REFERENCES `asignaturas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.examenes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `examenes` DISABLE KEYS */;
INSERT IGNORE INTO `examenes` (`id`, `create_at`, `nombre`, `asignatura_id`) VALUES
	(1, '2020-12-23 11:53:28.000000', 'Examen de historia', NULL);
/*!40000 ALTER TABLE `examenes` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.preguntas
CREATE TABLE IF NOT EXISTS `preguntas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `examen_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9hlw51x7hfqs1tv3sviwqycqi` (`examen_id`),
  CONSTRAINT `FK9hlw51x7hfqs1tv3sviwqycqi` FOREIGN KEY (`examen_id`) REFERENCES `examenes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.preguntas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `preguntas` DISABLE KEYS */;
INSERT IGNORE INTO `preguntas` (`id`, `texto`, `examen_id`) VALUES
	(1, 'Descubridor de América', 1),
	(3, 'Descubridor de América, nacionalidad', 1),
	(4, 'Descubridor de América, siglo', 1);
/*!40000 ALTER TABLE `preguntas` ENABLE KEYS */;

-- Volcando estructura para tabla db_microservicios_examenes.respuestas
CREATE TABLE IF NOT EXISTS `respuestas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `texto` varchar(255) DEFAULT NULL,
  `alumno_id` bigint(20) DEFAULT NULL,
  `pregunta_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKptndav2td8pqi7e51ihgq95gp` (`pregunta_id`),
  CONSTRAINT `FKptndav2td8pqi7e51ihgq95gp` FOREIGN KEY (`pregunta_id`) REFERENCES `preguntas` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla db_microservicios_examenes.respuestas: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `respuestas` DISABLE KEYS */;
INSERT IGNORE INTO `respuestas` (`id`, `texto`, `alumno_id`, `pregunta_id`) VALUES
	(1, 'Cristóbal Colón', 3, 1),
	(2, 'Española', 3, 3),
	(3, 'XV', 3, 4);
/*!40000 ALTER TABLE `respuestas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
