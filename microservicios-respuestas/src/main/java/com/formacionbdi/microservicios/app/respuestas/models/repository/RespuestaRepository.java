package com.formacionbdi.microservicios.app.respuestas.models.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.formacionbdi.microservicios.app.respuestas.models.entity.Respuesta;

public interface RespuestaRepository extends MongoRepository<Respuesta, Long>{

	@Query("{'alumnoId': ?0, 'preguntaId': { $in: ?1}}")
	public Iterable<Respuesta> findRespuestaByAlumnoByPreguntaIds(Long alumnoId, Iterable<Long> preguntaids);

	@Query("{'alumnoId': ?0}")
	public Iterable<Respuesta> findByAlumnoId(Long alumnoId);
	
	//	@Query("select r from Respuesta r "
//			+ "join fetch r.pregunta p "
//			+ "join fetch p.examen e "
//			+ "where r.alumnoId =?1 and e.id=?2")
//	public Iterable<Respuesta> findRespuestaByAlumnoByExamen(Long alumnoId, Long examenId);


	

}
