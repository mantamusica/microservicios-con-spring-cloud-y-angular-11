-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.5.8-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla db_microservicios_examenes.alumnos: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO alumnos (id, apellido, create_at, email, nombre, foto) VALUES
	(1, 'Vázquez', '2020-12-22 11:32:14.000000', 'hv@gmail.com', 'Hernando', NULL),
	(2, 'López', '2020-12-22 11:33:49.000000', 'al@gmail.com', 'Alejandro', NULL),
	(3, 'Fernández', '2020-12-22 11:39:05.000000', 'lf@gmail.com', 'Luis', NULL),
	(4, 'Fernández', '2020-12-22 12:16:11.000000', 'mf@gmail.com', 'Manuel', NULL),
	(5, 'Fernández', '2020-12-23 17:05:06.000000', 'af@gmail.com', 'Aaron', NULL),
	(6, 'Fernández', '2020-12-23 17:05:12.000000', 'cf@gmail.com', 'Carlos', NULL),
	(7, 'Fuentes', '2020-12-23 17:05:18.000000', 'cf@gmail.com', 'Carlos', NULL),
	(8, 'Diez', '2020-12-23 17:05:28.000000', 'cd@gmail.com', 'Carlos', NULL),
	(9, 'Diez', '2020-12-23 17:05:39.000000', 'pd@gmail.com', 'Pato', NULL),
	(10, 'Fuente', '2020-12-23 17:05:50.000000', 'ff@gmail.com', 'Fernando', NULL),
	(11, 'Fuente', '2020-12-23 17:05:59.000000', 'cf@gmail.com', 'Chema', NULL),
	(12, 'Perez', '2020-12-24 09:59:49.000000', 'jp@gmail.com', 'Jano', NULL);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
