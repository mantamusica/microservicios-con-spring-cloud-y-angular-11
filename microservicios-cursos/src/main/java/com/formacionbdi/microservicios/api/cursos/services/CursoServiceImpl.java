/**
 * 
 */
package com.formacionbdi.microservicios.api.cursos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacionbdi.microservicios.api.cursos.clients.AlumnoFeignClient;
import com.formacionbdi.microservicios.api.cursos.clients.RespuestaFeignClient;
import com.formacionbdi.microservicios.api.cursos.models.entity.Curso;
import com.formacionbdi.microservicios.api.cursos.models.repository.CursoRepository;
import com.formacionbdi.microservicios.commons.alumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.services.CommonServiceImpl;

/**
 * @author jcagigas
 *
 */
@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursoServiceI {

	@Autowired
	private RespuestaFeignClient respuestaClient;
	
	@Autowired
	private AlumnoFeignClient alumnoClient;
	
	@Override
	@Transactional(readOnly = true)
	public Curso findCursoByAlumnoId(Long id) {
		return repository.findCursoByAlumnoId(id);
	}

	@Override
	public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId) {
		return respuestaClient.obtenerExamenesIdsConRespuestasAlumno(alumnoId);
	}

	@Override
	public Iterable<Alumno> obtenerAlumnosPorCurso(List<Long> ids) {
		return alumnoClient.obtenerAlumnosPorCurso(ids);
	}

	@Override
	@Transactional
	public void eliminarCursoAlumnoPorId(Long id) {
		repository.eliminarCursoAlumnoPorId(id);
		
	}

}
