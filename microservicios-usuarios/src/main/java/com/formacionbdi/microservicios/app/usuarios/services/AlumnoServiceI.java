package com.formacionbdi.microservicios.app.usuarios.services;


import java.util.List;

import com.formacionbdi.microservicios.commons.alumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.services.CommonServiceI;

public interface AlumnoServiceI extends CommonServiceI<Alumno> {

	public List<Alumno> findByNombreOrApellido(String value);
	
	public Iterable<Alumno> findAllById(Iterable<Long> ids);

	public void eliminarCursoAlumnoPorId(Long id);

}
