package com.formacionbdi.microservicios.api.examenes.services;

import java.util.List;

import com.formacionbdi.microservicios.commons.examenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.models.entity.Examen;
import com.formacionbdi.microservicios.commons.services.CommonServiceI;

public interface ExamenServiceI extends CommonServiceI<Examen>{

	public List<Examen> findByNombre(String value);
	
	public Iterable<Asignatura> findAllAsignaturas();
	
	public Iterable<Long> findExamenesIdsConRespuestasByPreguntaIds(Iterable<Long> preguntaIds);
}
